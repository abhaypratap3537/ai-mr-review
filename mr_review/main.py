# TODO: 500ステップまでの変更にのみレビューを行う

from azure_openai_service.get_ai_review import getAiReview
from azure_openai_service.api import azure_open_ai_gpt_4_model
from gitlab.prepare_messages_and_code_changes import (
    prepareMessagesAndCodeChanges,
)
from gitlab.reflect_review_result import reflectReviewResult

# MRからユーザー指定のメッセージと変更内容を取得
system_message, user_message, code_changes = prepareMessagesAndCodeChanges()

# AIからレビューを得る
review_result = getAiReview(
    azure_open_ai_gpt_4_model, system_message, user_message, code_changes
)

# レビュー結果をMRに投稿する
comment_response = reflectReviewResult(azure_open_ai_gpt_4_model, review_result)

print("レビュー結果")
print(review_result)
