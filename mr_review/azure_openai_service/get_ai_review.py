from azure_openai_service.api.request import request
from azure_openai_service.create_request_messages import createRequestMessages


def getAiReview(
    model: str,
    user_specified_system_message: str,
    user_specified_user_message: str,
    code_changes: str,
):
    system_message, user_message = createRequestMessages(
        user_specified_system_message, user_specified_user_message
    )

    review_result = request(model, system_message, user_message, code_changes)

    return review_result
