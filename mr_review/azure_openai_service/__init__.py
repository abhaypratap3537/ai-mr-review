ai_review_default_system_message = (
    "あなたはプログラミングのスペシャリストでコードレビューをするアシスタントです。"
)

ai_review_common_system_message = (
    "レスポンスには適切に改行を含めてください。" + "改行は'\n'として出力してください"
)

ai_review_default_user_message = (
    "次のコード変更を分析し、修正が必要な問題がある場合は見つけます。 "
)

ai_review_common_user_message = "コードの変更は git diff 表記で行われ、- で始まる行が削除され、+ で始まる行が追加されます。"
