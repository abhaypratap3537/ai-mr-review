from . import (
    ai_review_default_system_message,
    ai_review_common_system_message,
    ai_review_default_user_message,
    ai_review_common_user_message,
)


def createRequestMessages(specified_system_message: str, specified_user_message: str):
    customizable_system_message = (
        specified_system_message
        if specified_system_message != ""
        else ai_review_default_system_message
    )
    system_message = customizable_system_message + ai_review_common_system_message

    customizable_user_message = (
        specified_user_message
        if specified_user_message != ""
        else ai_review_default_user_message
    )
    user_message = customizable_user_message + ai_review_common_user_message

    return system_message, user_message
