import os
from openai import OpenAI


def request(
    model: str, system_message: str, user_message: str, code_changes: str
):
    # OpenAIのAPI_KEYを指定します
    client = OpenAI(
        # This is the default and can be omitted
        api_key=os.environ.get("OPENAI_ACCESS_KEY"),
    )
    # OpenAIにリクエスト
    response = client.chat.completions.create(
        model=model,
        messages=[
            {
                "role": "system",
                "content": system_message,
            },
            {
                "role": "user",
                "content": f"{user_message} : {code_changes}",
            },
        ],
        max_tokens=2000,
    )

    # レビュー結果を抽出
    review_result = response.choices[0].message.content

    return review_result
