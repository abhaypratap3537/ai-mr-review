from gitlab.api.get_mr_changes import fetchGitLabMRChanges
from gitlab.api.get_mr_overview import fetchGitLabMROverview
from gitlab.extract_system_user_messages_from_description import (
    extractSystemUserMessagesFromDescription,
)


def prepareMessagesAndCodeChanges():
    # MRの概要取得
    mr_overview_data = fetchGitLabMROverview().json()
    # 説明欄
    mr_description = mr_overview_data["description"]
    # 説明欄からAIレビューへのプロンプトを抽出
    system_message, user_message = extractSystemUserMessagesFromDescription(
        mr_description
    )

    # MRの情報をJSONデータとして取得します
    mr_changes_data = fetchGitLabMRChanges().json()
    # 変更内容
    code_changes = mr_changes_data["changes"]

    return system_message, user_message, code_changes
