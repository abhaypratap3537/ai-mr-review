import re


def extractSystemUserMessagesFromDescription(description: str):
    try:
        ai_review_chapter = _extract(description, "## AIレビュー")
    except ValueError as e:
        print(
            f"次のため、ユーザー指定のシステムメッセージとユーザーメッセージは未入力とします：{e}"
        )
        return "", ""

    system_message = ""
    user_message = ""
    try:
        system_message = _extract(ai_review_chapter, "### システムメッセージ")
    except ValueError as e:
        print(f"{e}")

    try:
        user_message = _extract(ai_review_chapter, "### ユーザーメッセージ")
    except ValueError as e:
        print(f"{e}")

    print(f"以下のメッセージとして入力します")
    print(f"システムメッセージ：{system_message}")
    print(f"ユーザーメッセージ：{user_message}")
    return system_message, user_message


def _extract(text: str, pattern: str):
    hashtag_count = _count_starting_hashtags(pattern)

    # 正規表現で章を抽出
    chapter_pattern = re.compile(rf"^{pattern}$", re.MULTILINE)
    match = chapter_pattern.search(text)

    if match:
        start_index = match.end()  # マッチしたパターンの終了位置を取得

        # マッチしたパターンの終了位置以降のテキスト
        text_after_start_index = text[start_index:]
        end_index = -1
        for i in range(hashtag_count, 0, -1):
            hashes = "#" * i
            # 次に自身と同じレベルかそれより次元の低い見出し位置を探す
            next_chapter_pattern = re.compile(f"^{hashes} .+$", re.MULTILINE)
            match2 = next_chapter_pattern.search(text_after_start_index)
            if match2:
                end_index = match2.start() + start_index
                break

        if end_index == -1:
            # 最後の章の場合
            extracted_text = text[start_index:]
        else:
            extracted_text = text[start_index:end_index]

        return extracted_text
    else:
        raise ValueError(f"「{pattern}」が見つかりません")


def _count_starting_hashtags(text: str):
    """
    Counts the number of consecutive '#' characters at the beginning of the given text.

    Args:
        text (str): The input text.

    Returns:
        int: The count of consecutive '#' characters at the start.
    """
    # Remove leading spaces and count consecutive '#' characters
    return len(text.lstrip().split()[0])
