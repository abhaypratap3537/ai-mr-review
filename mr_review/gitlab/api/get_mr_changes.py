import requests
from . import *


def fetchGitLabMRChanges():
    # マージリクエストの変更内容を取得するAPIエンドポイントを構築します
    gitlab_api_url = (
        f"{gitlab_host}/projects/{project_id}/merge_requests/{merge_request_id}/changes"
    )

    # APIリクエストを送信してマージリクエストの変更内容を取得します
    mr_changes_response = requests.get(gitlab_api_url, headers=gitlab_api_headers)

    return mr_changes_response
