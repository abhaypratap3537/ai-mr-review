import os

# GitLabのURL、アクセストークン、プロジェクトID、マージリクエストのIDを指定します
gitlab_host = os.environ.get("CI_API_V4_URL")  # https://gitlab.com/api/v4
# project_id = os.environ.get("CI_PROJECT_ID")
# merge_request_id = os.environ.get("CI_MERGE_REQUEST_IID")
project_id = os.environ.get("TRIGGER_PROJECT_ID")
merge_request_id = os.environ.get("TRIGGER_MERGE_REQUEST_IID")

# リクエストヘッダーにアクセストークンを設定します
# gitlab_api_headers = {"PRIVATE-TOKEN": os.environ.get("ACCESS_KEY")}
gitlab_api_headers = {"PRIVATE-TOKEN": os.environ.get("TRIGGER_ACCESS_KEY")}
