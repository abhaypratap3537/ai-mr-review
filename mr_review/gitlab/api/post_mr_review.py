import requests
from . import *


def postMRReview(review_result: str):

    # Make comment on merge request with the analysis
    comment_url = f"{gitlab_host}/projects/{project_id}/merge_requests/{merge_request_id}/discussions"
    comment_data = {"body": review_result}
    comment_response = requests.post(
        comment_url, headers=gitlab_api_headers, data=comment_data
    )

    return comment_response
