import requests
from . import *


def fetchGitLabMROverview():
    # マージリクエストの概要を取得するAPIエンドポイントを構築します
    gitlab_api_url = (
        f"{gitlab_host}/projects/{project_id}/merge_requests/{merge_request_id}"
    )

    # APIリクエストを送信してマージリクエストの概要を取得します
    mr_overview_response = requests.get(gitlab_api_url, headers=gitlab_api_headers)

    return mr_overview_response
